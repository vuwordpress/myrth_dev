<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'myrth');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{23t!QS>mGdTf!Do5ZRglx>w=U/oJ0T!`zE09OE5(_,VKU2Ju^l4L{%,CEa ##D8');
define('SECURE_AUTH_KEY',  '{~hO/<HyU4JD7xV;# eD1-Pg#7SsnNqA,AtK78BN<zIbB3GcvWp1cO:,G5upnZ[X');
define('LOGGED_IN_KEY',    'CgK,GXU@&Kk{,>I-m$:_QHC5S3bjUA)/8nV]_h(g5:_t=X+JOj(Rnow`F!Xh&UB|');
define('NONCE_KEY',        '(C?fLFDvjpG `Rt08[BOh([`[]jkqFiLE+>{;iB:HnSFiI%t+8CIy=5,cY(J~>C|');
define('AUTH_SALT',        ']ZeE&ndBD!GEb&QY41BB1cSl/J;>?I R$upBWv8X&Ho&R*HJk6]]TBEgoFve|j>0');
define('SECURE_AUTH_SALT', '7G^Cb.|}f0:hX&zpQMg`Iq1@!ti?Yq0G^UOtJa$&>[b]) ]J4N@<`^e.Q2!}i$:R');
define('LOGGED_IN_SALT',   '*R%pQiD8ycM1G2dkJ6zY,R`{{@+9%5gx@0/x%V@hBn{?O)I|gU}:vVKw=8|39+!l');
define('NONCE_SALT',       'cgDRmc(}FY-mo?psm8fAlwBmFGX]RPGDV?@hX#R7~,jXAXd1q1_^9a%jpejtSvy4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'web_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
