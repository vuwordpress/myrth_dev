<!-- Start Footer Area -->
<footer id="htc__footer">
    <!-- Start Footer Widget -->
    <div class="footer__container bg__cat--1">
        <div class="container">
            <div class="row">
                <!-- Start Single Footer Widget -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="footer">
                        <h2 class="title__line--2">company info</h2>
                        <div class="ft__details">
                            <p>Established in 2018, Myrth is created with an aim to provide chic and stylish everyday wear; for office and for casual weekend wears.</p>
                            <div class="footer__btn">
                                <a href="<?php echo home_url() ?>/about-myrth">read more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Footer Widget -->
                <!-- Start Single Footer Widget -->
                <div class="col-md-3 col-sm-6 col-xs-12 xmt-40">
                    <div class="footer">
                        <h2 class="title__line--2">information</h2>
                        <div class="ft__inner">
                            <ul class="ft__list">
                                <li><a href="<?php echo home_url() ?>/about-myrth/">About us</a></li>
                                <li><a href="<?php echo home_url() ?>/return-and-exchange/">Return and Exchange</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Footer Widget -->
                <!-- Start Single Footer Widget -->
                <div class="col-md-3 col-sm-6 col-xs-12 xmt-40 smt-40">
                    <div class="footer">
                        <h2 class="title__line--2">my account</h2>
                        <div class="ft__inner">
                            <ul class="ft__list">
                                <?php 
                                $current_user = wp_get_current_user();
                                $text = ($current_user->ID > 0) ? 'My Account' : 'Signup / Login';
                                ?>
                                <li><a href="<?php echo home_url() ?>/my-account/"><?php echo $text; ?></a></li>
                                <li><a href="<?php echo home_url() ?>/cart/">My Cart</a></li>
                                <li><a href="<?php echo home_url() ?>/wishlist/">Wishlist</a></li>
                                <li><a href="<?php echo home_url() ?>/checkout">Checkout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Footer Widget -->
                <!-- Start Single Footer Widget -->
<!--                <div class="col-md-2 col-sm-6 col-xs-12 xmt-40 smt-40">
                    <div class="footer">
                        <h2 class="title__line--2">Our service</h2>
                        <div class="ft__inner">
                            <ul class="ft__list">
                                <li><a href="#">My Account</a></li>
                                <li><a href="cart.html">My Cart</a></li>
                                <li><a href="#">Login</a></li>
                                <li><a href="wishlist.html">Wishlist</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>-->
                <!-- End Single Footer Widget -->
                <!-- Start Single Footer Widget -->
                <div class="col-md-3 col-sm-6 col-xs-12 xmt-40 smt-40">
                    <div class="footer">
                        <!-- <h2 class="title__line--2">Our country</h2>
                        <div class="ft__inner">
                            <ul class="lan__select">
                                <li><a href="#">United State</a>
                                    <ul class="drop__ountry">
                                        <li><a href="#">United State</a>
                                        <li><a href="#">United State</a>
                                        <li><a href="#">United State</a>
                                        <li><a href="#">United State</a>
                                    </ul>
                                </li>
                            </ul> -->
                        <div class="ft__social__link">
                            <span>follow us</span>
                            <ul class="social__link">
                                <li><a href="https://www.instagram.com/shopmyrth/" target="_blank"><i class="icon-social-instagram icons"></i></a></li>
                                <li><a href="https://www.facebook.com/ShopMyrth/" target="_blank"><i class="icon-social-facebook icons"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Footer Widget -->
        </div>
    </div>
</div>
<!-- End Footer Widget -->
<!-- Start Copyright Area -->
<div class="htc__copyright bg__white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="copyright__inner">
                    <p>Copyright© <a href="<?php echo home_url(); ?>" target="blank">MYRTH</a> 2018. All right reserved.</p>
                    <a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/others/shape/paypol.png" alt="payment images"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Copyright Area -->
</footer>

</div>
<!-- /wrapper -->

<?php wp_footer(); ?>

<!-- Placed js at the end of the document so the pages load faster -->

<!-- jquery latest version -->
<script src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery-1.12.0.min.js"></script>
<!-- Bootstrap framework js -->
<script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
<!-- All js plugins included in this file. -->
<script src="<?php echo get_template_directory_uri() ?>/js/plugins.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/owl.carousel.min.js"></script>
<script src="https://s3-ap-southeast-1.amazonaws.com/shop365/js/jquery.cookie.js"></script>

<!-- Google Map js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmGmeot5jcjdaJTvfCmQPfzeoG_pABeWo"></script>
<script>
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 12,

            scrollwheel: false,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(23.7286, 90.3854), // New York

            // How you would like to style the map. 
            // This is where you would paste any style found on Snazzy Maps.
            styles:
                    [{
                            "featureType": "all",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "saturation": 36
                                },
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 40
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "visibility": "on"
                                },
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 17
                                },
                                {
                                    "weight": 1.2
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 29
                                },
                                {
                                    "weight": 0.2
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 18
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                },
                                {
                                    "lightness": 19
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#141516"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        }
                    ]
        };

        // Get the HTML DOM element that will contain your map 
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('googleMap');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(23.7286, 90.3854),
            map: map,
            title: 'Ramble!',
            icon: 'images/icons/map.png',
            animation: google.maps.Animation.BOUNCE

        });
    }
</script>



<!-- Waypoints.min.js. -->
<script src="<?php echo get_template_directory_uri() ?>/js/waypoints.min.js"></script>
<!-- Main js file that contents all jQuery plugins activation. -->
<script src="<?php echo get_template_directory_uri() ?>/js/main.js"></script>

<!-- analytics -->
<script>
    (function (f, i, r, e, s, h, l) {
        i['GoogleAnalyticsObject'] = s;
        f[s] = f[s] || function () {
            (f[s].q = f[s].q || []).push(arguments)
        }, f[s].l = 1 * new Date();
        h = i.createElement(r),
                l = i.getElementsByTagName(r)[0];
        h.async = 1;
        h.src = e;
        l.parentNode.insertBefore(h, l)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
    ga('send', 'pageview');
</script>
</body>
</html>
