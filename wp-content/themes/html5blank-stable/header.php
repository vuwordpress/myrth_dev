<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php wp_title(''); ?><?php
            if (wp_title('', false)) {
                echo ' :';
            }
            ?> <?php bloginfo('name'); ?></title>
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

        <!-- All css files are included here. -->
        <!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css">
        <!-- Owl Carousel min css -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/owl.theme.default.min.css">
        <!-- This core.css file contents all plugings css file. -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/core.css">
        <!-- Theme shortcodes/elements style -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/shortcode/shortcodes.css">
        <!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css">
        <!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/responsive.css">
        <!-- User style -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/custom.css">


        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php bloginfo('description'); ?>">
		
        <?php wp_head(); ?>
<!--         <script src="http://shopmyrth.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4"></script> -->
		
		<script>
            // conditionizr.com
            // configure environment tests
            conditionizr.config({
                assets: '<?php echo get_template_directory_uri(); ?>',
                tests: {}
            });
        </script>
        <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a3cedde9d192f00137433de&product=inline-share-buttons"></script>
    </head>
    <body <?php body_class(); ?>>
        <div id="yith-wcwl-popup-message" style="display: none;"><div id="yith-wcwl-message"></div></div>
        <!-- wrapper -->
        <div class="wrapper">

            <!-- Start Header Style -->
            <header id="htc__header" class="htc__header__area header--one">
                <div class="header__top__area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                                <div class="ht__header__top__left">
<!--                                    <ul class="select__language hidden-sm hidden-xs">
                                        <li class="drop--lan"><a href="#">WELCOME</a>
                                        </li>
                                    </ul>-->
                                    <div class="htc__contact">
                                                                               <a style="color: #c991ba" href="mailto:enquiry@shopmyrth.com"><!--<i class="icon-call-out icons"></i>--><i class="fa fa-envelope"></i>enquiry@shopmyrth.com</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                                <div class="logo">
                                    <a href="<?php echo home_url() ?>">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/logo/eme.png" alt="logo images">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                                <ul class="ht__header__right">
                                    <?php 
                                    $current_user = wp_get_current_user();
                                    $text = ($current_user->ID > 0) ? 'My Account' : 'Signup / Login';
                                    ?>
                                    <li><a href="<?php echo home_url(); ?>/my-account"><?php echo $text; ?></a></li>
                                    <li><a href="<?php echo home_url(); ?>/wishlist">Wishlist</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Start Mainmenu Area -->
                <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="menumenu__container clearfix">
                                <div class="col-md-2 col-lg-2 col-sm-3 col-xs-3">
                                    <div class="header__search search search__open">
                                        <a href="#"><i class="icon-magnifier icons"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-6 col-xs-5">
                                    <nav class="main__menu__nav hidden-xs hidden-sm">
                                        <?php html5blank_nav(); ?>
                                    </nav>
                                    <div class="mobile-menu clearfix visible-xs visible-sm">
                                        <nav id="mobile_dropdown">
                                            <?php html5blank_nav_mobile(); ?>
                                        </nav>
                                    </div>  
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-3 col-xs-4">
                                    <div class="htc__shopping__cart">
                                        <a class="cart__menu" href="javascript:void(0)"><i class="icon-handbag icons"></i></a>
                                        <a href="javascript:void(0)"><span class="htc__qua"><?php echo wc()->cart->get_cart_contents_count(); ?></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mobile-menu-area"></div>
                    </div>
                </div>
                <!-- End Mainmenu Area -->
            </header>
            <!-- End Header Area -->

            <div class="body__overlay"></div>
            
            <!-- Start Offset Wrapper -->
            <div class="offset__wrapper">
                <!-- Start Search Popap -->
                <?php get_template_part('template-parts/searchform') ?>
                <!-- End Search Popap -->

                <!-- Start Cart Panel -->
                <?php get_template_part('template-parts/cart', 'panel') ?>    
                <!-- End Cart Panel -->
            </div>
            <!-- End Offset Wrapper -->