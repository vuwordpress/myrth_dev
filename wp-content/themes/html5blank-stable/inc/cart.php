<?php

add_action('wp_ajax_myrth_add_to_cart', 'myrth_add_to_cart');
add_action('wp_ajax_nopriv_myrth_add_to_cart', 'myrth_add_to_cart');

function myrth_add_to_cart() {
    global $woocommerce;
    
    if (!$_POST['attribute_pa_color'] && !$_POST['attribute_pa_size']) {
        $woocommerce->cart->add_to_cart($_POST['product_id'], 1);
        $product = get_post($_POST['product_id']);
        $title = $product->post_title;
    } else {
        $variation_product = myrth_get_product_variation_by_attr($_POST['product_id'], $_POST['attribute_pa_color'], $_POST['attribute_pa_size']);
        $variation = array();
        if ($_POST['attribute_pa_color']) {
            $variation['attribute_pa_color'] = $_POST['attribute_pa_color'];
        }
        if ($_POST['attribute_pa_size']) {
            $variation['attribute_pa_size'] = $_POST['attribute_pa_size'];
        }
        $woocommerce->cart->add_to_cart($_POST['product_id'], 1, $variation_product[0]->ID, $variation);
        $title = $variation_product[0]->post_title;
    }
    
    $msg = "“" .$title . '” has been added to your cart.';
    
    $html_notice = '<div class="woocommerce-message">' .  $msg . '<a href="' . home_url() . '/cart/" class="button wc-forward">View cart</a></div>';
    
    $cart_panel_html = load_panel_cart();
    
    $data = array(
        'html_notice' => $html_notice,
        'html_cart_panel' => $cart_panel_html,
        'count' =>  wc()->cart->get_cart_contents_count(),
        'add_to_cart' => 1
    );
    echo json_encode($data);
    die();
}

function load_panel_cart(){
    ob_start();
    include_once 'partials/cart-panel.php';
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_filter( 'woocommerce_checkout_fields' , 'myrth_rename_wc_checkout_fields' );
function myrth_rename_wc_checkout_fields($fields) {
    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing']['billing_company']['placeholder'] = 'Company';
    $fields['billing']['billing_phone']['placeholder'] = 'Phone';
    $fields['shipping']['shipping_first_name']['placeholder'] = 'First Name';
    $fields['shipping']['shipping_last_name']['placeholder'] = 'Last Name';
    $fields['shipping']['shipping_company']['placeholder'] = 'Company';
    return $fields;
}

add_action( 'wp_print_scripts', 'iconic_remove_password_strength', 10 );

function iconic_remove_password_strength() {
    wp_dequeue_script( 'wc-password-strength-meter' );
}
