<?php

add_action('wp_ajax_myrth_load_more', 'myrth_load_more');
add_action('wp_ajax_nopriv_myrth_load_more', 'myrth_load_more');
function myrth_load_more() {
    if ($_GET['page']) {
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => 12,
            'paged' => $_GET['page']
        );
        $posts = get_posts($args);
        if ($posts) {
            $arr = array();
            foreach ($posts as $key => $post_item) {
                $regular_price = get_post_meta($post_item->ID, '_regular_price', true);
                $sale_price = get_post_meta($post_item->ID, '_sale_price', true);
                if (!$regular_price) {
                    $regular_price = get_post_meta($post_item->ID, '_price', true);
                }
                $product = new WC_Product_Variable($post_item->ID);
                $attributes = $product->get_attributes();
                $data_attributes = _prepare_data_attributes($attributes);
                $gallery = _prepare_data_gallery($post_item->ID);
                $item = array(
                    'product_id' => $post_item->ID,
                    'title' => $product->get_title(),
                    'regular_price' => $regular_price,
                    'sale_price' => $sale_price,
                    'data_attributes' => $data_attributes,
                    'images' => $gallery
                );
                $arr[] = $item;
            }
            $html = generate_html($arr);
            $data = array(
                'status' => true,
                'html' => $html
            );
            echo json_encode($data);
            die();
        }
    }
    $data = array('status' => false);
    echo json_encode($data);
    die();
}

function generate_html($posts){
    $list_products = $posts;
    ob_start();
    include('ajax.php');
    $html = ob_get_clean();
    return $html;
}

/* Custom query: query with paremeter */
add_action('pre_get_posts', 'myrth_additional_woo_query');

function myrth_additional_woo_query($query) {
    
    if (is_search()) {
        $query->set( 'post_type', array( 'product') );
        return $query;
    }
    
    if (is_product_category() || is_post_type_archive('product')) {
        if ($_GET['color']) {
            $taxquery = array(
                array(
                    'taxonomy' => 'pa_color',
                    'field' => 'slug',
                    'terms' => $_GET['color']
                )
            );
            
            $base_tax_query = $query->query_vars['tax_query'];
            $base_tax_query[1] = $taxquery;
            $query->set('tax_query', $base_tax_query);
        } elseif ($_GET['size']) {
            $taxquery = array(
                array(
                    'taxonomy' => 'pa_size',
                    'field' => 'slug',
                    'terms' => $_GET['size']
                )
            );
            $base_tax_query = $query->query_vars['tax_query'];
            $base_tax_query[1] = $taxquery;
            $query->set('tax_query', $base_tax_query);
        }
        
        if ($_GET['min_price'] && $_GET['max_price']) {
            $arr_price = array($_GET['min_price'], $_GET['max_price']);
            $meta_query = array(
                'relation' => 'OR',
                array(
                    'key' => '_regular_price',
                    'value' => $arr_price,
                    'compare' => 'BETWEEN',
                    'type' => 'DECIMAL'
                ),
                array(
                    'key' => '_sale_price',
                    'value' => $arr_price,
                    'compare' => 'BETWEEN',
                    'type' => 'DECIMAL'
                ),
                array(
                    'key' => '_price',
                    'value' => $arr_price,
                    'compare' => 'BETWEEN',
                    'type' => 'DECIMAL'
                )
            );
            $query->set('meta_query', $meta_query);
        }
    }
}

add_filter('nav_menu_css_class', 'myrth_menu_classes', 1, 3);

function myrth_menu_classes($classes, $item, $args) {
    if ($args->theme_location == 'header-menu') {
        $classes[] = 'drop';
    }
    return $classes;
}

add_filter('wp_nav_menu', 'myrth_submenu_class');

function myrth_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/', '/ class="dropdown" /', $menu);
    return $menu;
}

function myrth_get_product_variation($product_id) {
    global $wpdb;
    $product_variations = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}posts WHERE post_parent = $product_id AND post_status = 'publish'", OBJECT );
    return $product_variations;
}

function myrth_get_product_variation_by_attr($product_id, $color, $size) {

    $args1 = NULL;
    if ($color) {
        $args1 = array(
            'key' => 'attribute_pa_color',
            'value' => $color,
            'compare' => '=',
        );
    }

    $args2 = NULL;
    if ($size) {
        $args2 = array(
            'key' => 'attribute_pa_size',
            'value' => $size,
            'compare' => '=',
        );
    }

    $args = array(
        'post_type' => 'product_variation',
        'post_parent' => $product_id,
        'meta_query' => array(
            'relation' => 'AND',
            $args1, $args2
        ),
        'posts_per_page' => -1,
    );

    $post = get_posts($args);
    return $post;
}

function myrth_get_list_attribute_item($attribute_name) {
    global $product;
    $attributes = $product->get_attributes();
    if ($attributes) {
        $arr = array();
        foreach ($attributes as $att) {
            if ($att['data']['name'] == $attribute_name) {
                if ($att['data']['options']) {
                    foreach ($att['data']['options'] as $key => $val) {
                        $term = get_term($val);
                        $arr[] = $term;
                    }
                }
            }
        }
        return $arr;
    }
}

function get_news_product() {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 12
    );
    $posts = get_posts($args);
    if ($posts) {
        $arr = array();
        foreach ($posts as $key => $post_item) {
            $regular_price = get_post_meta($post_item->ID, '_regular_price', true);
            $sale_price = get_post_meta($post_item->ID, '_sale_price', true);
            if (!$regular_price) {
                $regular_price = get_post_meta($post_item->ID, '_price', true);
            }
            $product = new WC_Product_Variable($post_item->ID);
            $attributes = $product->get_attributes();
            $data_attributes = _prepare_data_attributes($attributes);
            $gallery = _prepare_data_gallery($post_item->ID);
            $item = array(
                'product_id' => $post_item->ID,
                'title' => $product->get_title(),
                'regular_price' => $regular_price,
                'sale_price' => $sale_price,
                'data_attributes' => $data_attributes,
                'images' => $gallery
            );
            $arr[] = $item;
        }
        return $arr;
    }
}

function _prepare_data_gallery($product_id) {
    $product_variations = myrth_get_product_variation($product_id);
    $arr_gallery = array();
    foreach ($product_variations as $key => $item) {
        $color = get_post_meta($item->ID, 'attribute_pa_color', true);
        $thumbnail_id = get_post_meta($item->ID, '_thumbnail_id', true);
        $large_image = wp_get_attachment_image_src($thumbnail_id, 'full');
        $arr_gallery[$color] = $large_image[0];
    }
    return $arr_gallery;
}

function _prepare_data_attributes($arr_attributes) {
    $arr = array();
    foreach ($arr_attributes as $key => $attr_item) {
        foreach ($attr_item['data']['options'] as $index => $val) {
            $term = get_term($val);
            $arr_item = array(
                'slug' => $term->slug,
                'name' => $term->name,
                'term_id' => $term->term_id
            );
            $arr[$key][] = $arr_item;
        }
    }
    return $arr;
}

function myrth_get_pro__color($product_data) {
    if ($product_data['data_attributes']) {
        if ($product_data['data_attributes']['pa_color']) {
            $arr = array();
            foreach ($product_data['data_attributes']['pa_color'] as $key => $item) {
                $arr[$item['slug']] = array(
                    'slug' => $item['slug'],
                    'image' => $product_data['images'][$item['slug']]
                );
            }
            return $arr;
        }
    }
}

function myrth_show_attributes($attributes) {
    $html_item = '';
    foreach ($attributes as $key => $val) {
        $term = get_term_by('slug', $val, $key);
        if ($key == 'pa_color') {
            $prefix = 'Color';
        } else if ($key == 'pa_size') {
            $prefix = 'Size';
        }
        $html_item .= '<dt class="variation-' . $prefix . '">' . $prefix . ': <span>' . $term->name . '</span></dt>';
    }
    $html = '<dl class="variation">' . $html_item . '</dl>';
    return $html;
}

add_action('wp_head', 'pluginname_ajaxurl');

function pluginname_ajaxurl() {
    ?>
    <script type="text/javascript">
        var path_ajax = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <?php
}
