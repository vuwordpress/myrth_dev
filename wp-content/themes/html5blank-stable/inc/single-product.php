<?php

add_action('myrth_show_price', 'myrth_show_price', 10, 2);

function myrth_show_price($product_id) {
    $regular_price = get_post_meta($product_id, '_regular_price', true);
    $sale_price = get_post_meta($product_id, '_sale_price', true);
    if (!$regular_price) {
        $regular_price = get_post_meta($product_id, '_price', true);
    }
    $html_sale_price = '';
    if ($sale_price) {
        $html_sale_price = '<li>' . wc_price($sale_price) . '</li>';
        $html_regular_price = '<li class="old__prize">' . wc_price($regular_price) . '</li>';
    } else {
        $html_regular_price = '<li>' . wc_price($regular_price) . '</li>';
    }
    $result = '<ul class="pro__prize">' . $html_regular_price . $html_sale_price . '</ul>';
    echo $result;
}

add_action('myrth_show_color_variation', 'myrth_show_color_variation', 10, 2);

function myrth_show_color_variation($product_id) {
    global $product;
    $li = '';
    $attributes = $product->get_attributes();
    foreach ($attributes as $att) {
        if ($att['data']['name'] == 'pa_color') {
            if ($att['data']['options']) {
                foreach ($att['data']['options'] as $key => $val) {
                    $term = get_term($val);
                    $li .= '<li class="' . $term->slug . '"><a data-term-id="' . $term->term_id . '" data-color="' . $term->slug . '" href="javascript:void(0)">' . $term->name . '</a></li>';
                }
            }
        }
    }

    if ($li) {
        $html = '<div class="sin__desc align--left">
                    <p><span>color:</span></p>
                    <ul class="pro__color">' . $li . '</ul>
                </div><input id="attr_color" type="hidden" name="pa_color" value="" />';
        echo $html;
    }
}

add_action('myrth_show_size_variation', 'myrth_show_size_variation', 10, 2);

function myrth_show_size_variation($product_id) {
    global $product;
    $attributes = $product->get_attributes();
    $option = '';
    foreach ($attributes as $att) {
        if ($att['data']['name'] == 'pa_size') {
            if ($att['data']['options']) {
                foreach ($att['data']['options'] as $key => $val) {
                    $selected = ($key == 0) ? 'selected = "selected"' : '';
                    $term = get_term($val);
                    $option .= '<option ' . $selected . ' value="' . $term->slug . '">' . $term->name . '</option>';
                }
            }
        }
    }

    if ($option) {
        //$option = '<option value="">Size</option>' . $option;
        $option = $option;
        $html = '<div class="sin__desc align--left">
                                <p><span>size</span></p>
                                <select class="select__size">' . $option . '</select>
                            </div><input id="attr_size" type="hidden" name="pa_size" value="" />';
        echo $html;
    }
}

add_action('myrth_show_product_gallery', 'myrth_show_product_gallery', 10, 2);

function myrth_show_product_gallery($product_id) {
    global $product;

    //$list_attributes =  myrth_get_list_attribute_item('pa_color');
    $product_variations = myrth_get_product_variation($product_id);
    $small_img_html = '';
    $xhtml_product_small_img = '';
    if (!$product_variations) {
        $product_obj = wc_get_product($product_id);
        $attachment_ids = $product->get_gallery_attachment_ids();
        $index = 0;
        if ($attachment_ids) {
            foreach ($attachment_ids as $key => $attachment_id ) {
                $index ++;
                $class = ($index == 1) ? 'in active' : '';
                $src = wp_get_attachment_url($attachment_id, 'full');
                $large_img_html .= '<div role="tabpanel" class="tab-pane fade ' . $class . '" id="img-tab-' . $key . '">
                                            <img src="' . $src . '" alt="full-image">
                                   </div>';
                $small_img_html .= '<li style="display: inline-block" role="presentation" class="pot-small-img '  .$class . '">
                                        <a href="#img-tab-' . $key . '" role="tab" data-toggle="tab">
                                            <img src="' . $src . '" alt="small-image">
                                        </a>
                                    </li>';
            }
            $xhtml_product_big_img = '<div class="portfolio-full-image tab-content">' . $large_img_html . '</div>';
            $xhtml_product_small_img = '<ul class="product__small__images" role="tablist">' . $small_img_html . '</ul>';
        } else {
            $large_img_html .= '<div role="tabpanel" class="tab-pane active">' . get_the_post_thumbnail($product_id, 'full') . '</div>';
            $xhtml_product_big_img = '<div class="portfolio-full-image tab-content">' . $large_img_html . '</div>';
        }
        
        $xhtml = '<div class="htc__product__details__tab__content">' . $xhtml_product_big_img . $xhtml_product_small_img . '</div>';
        echo $xhtml;
        return;
    }
    $arr_variation_id = array();
    $arr_gallery = array();
    foreach($product_variations as $key => $item) {
        $color = get_post_meta($item->ID, 'attribute_pa_color', true);
        $size = get_post_meta($item->ID, 'attribute_pa_size', true);
        $thumbnail_id = get_post_meta($item->ID, '_thumbnail_id', true);
        $str_additional_variation_images = get_post_meta($item->ID, '_wc_additional_variation_images', true);
        if ($str_additional_variation_images) {
            $arr_additional_variation_images_id = explode(',', $str_additional_variation_images);
            foreach($arr_additional_variation_images_id as $key => $val) {
                $large_image = wp_get_attachment_image_src($val, 'full');
                $small_image = wp_get_attachment_image_src($val, 'custom-size-gll-full');
                if ($color && $size) {
                    $arr_gallery[$color][$size][] = array(
                        'large' => $large_image[0],
                        'small' => $small_image[0]
                    );
                } elseif ($color) {
                    $arr_gallery[$color][] = array(
                        'large' => $large_image[0],
                        'small' => $small_image[0]
                    );
                }
            }
        } else {
            $thumbnail_id = get_post_meta($item->ID, '_thumbnail_id', true);
            $large_image = wp_get_attachment_image_src($thumbnail_id, 'full');
            $small_image = wp_get_attachment_image_src($thumbnail_id, 'custom-size-gll-full');
            if ($color && $size) {
                $arr_gallery[$color][$size][] = array(
                    'large' => $large_image[0],
                    'small' => $small_image[0]
                );
            } elseif ($color) {
                $arr_gallery[$color][] = array(
                    'large' => $large_image[0],
                    'small' => $small_image[0]
                );
            }
        }
    }

    $xhtml_product_big_img = '';
    $xhtml_product_small_img = '';
    if ($arr_gallery) {
        $large_img_html = '';
        $small_img_html = '';
        $index = 0;
        if ($color && $size) {
            $i = 0;
            foreach($arr_gallery as $key => $arr_data) {
                foreach($arr_data as $slug => $arr_item) {
                    $i++;
                    $display = ($i == 1) ? 'style="display: inline-block"' : '';
                    foreach( $arr_item as $k => $item) {
                        $index ++;
                        $class = ($index == 1) ? 'in active' : '';
                        $class_small = ($index == 1) ? 'active' : '';
                        $large_img_html .= '<div data-color="' . $key . '-' . $k . '" data-size="' . $slug . '-' . $k . '" role="tabpanel" class="tab-pane fade ' .  $class . '" id="img-tab-' . $key . '-' . $slug . '-' . $k . '">
                                            <img src="' . $item['large'] . '" alt="full-image">
                                        </div>';
                        $small_img_html .= '<li ' . $display . ' id="nav-' . $key .  '-' . $slug . '-' . $k . '" data-color="' . $key . '" data-size="' . $slug . '" role="presentation" class="pot-small-img ' . $class_small . '">
                                            <a href="#img-tab-' . $key . '-' . $slug . '-' . $k . '" role="tab" data-toggle="tab">
                                                <img src="' . $item['small'] . '" alt="small-image">
                                            </a>
                                        </li>';
                    }
                }
            }
        } elseif ($color) {
            $i = 0;
            foreach($arr_gallery as $key => $arr_images) {
                $i++;
                $display = ($i == 1) ? 'style="display: inline-block"' : '';
                foreach($arr_images as $k => $item) {
                    $index ++;
                    $class = ($index == 1) ? 'in active' : '';
                    $class_small = ($index == 1) ? 'active' : '';
                    $large_img_html .= '<div data-color="' . $key . '-' . $k . '" role="tabpanel" class="tab-pane fade ' .  $class . '" id="img-tab-' . $key . '-' . $k . '">
                                            <img src="' . $item['large'] . '" alt="full-image">
                                        </div>';
                    $small_img_html .= '<li ' . $display . ' id="nav-' . $key .  '-' . $k . '" data-color="' . $key . '" role="presentation" class="pot-small-img ' . $class_small . '">
                                            <a href="#img-tab-' . $key . '-' . $k . '" role="tab" data-toggle="tab">
                                                <img src="' . $item['small'] . '" alt="small-image">
                                            </a>
                                        </li>';
                }
            }
        }
        
        $xhtml_product_big_img = '<div class="portfolio-full-image tab-content">' . $large_img_html . '</div>';
        if (count($arr_gallery) > 1) {
            $xhtml_product_small_img = '<ul class="product__small__images" role="tablist">' . $small_img_html . '</ul>';
        }
        
        $xhtml = '<div class="htc__product__details__tab__content">' . $xhtml_product_big_img . $xhtml_product_small_img .  '</div>';
        echo $xhtml;
    }   
}

add_action('myrth_show_button_add_to_cart', 'myrth_show_button_add_to_cart', 10, 2);
function myrth_show_button_add_to_cart($product_id){
    
}

add_action('myrth_show_categories_assigned', 'myrth_show_categories_assigned', 10, 2);

function myrth_show_categories_assigned($product_id) {
    $terms = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => false));
    if ($terms) {
        foreach($terms as $key => $term) {
            $li .= '<li><a href="' . get_term_link($term) . '">' . $term->name . ',</a></li>';
        }
        $html = '<div class="sin__desc align--left">
                                <p><span>Categories:</span></p>
                                <ul class="pro__cat__list">' . $li . '</ul>
                            </div>';
    }
    
    echo $html;
}

add_action('myrth_show_product_tag', 'myrth_show_product_tag', 10, 2);

function myrth_show_product_tag($product_id) {
    $html = '<div class="sin__desc align--left">
                                <p><span>Product tags:</span></p>
                                <ul class="pro__cat__list">
                                    <li><a href="#">Fashion,</a></li>
                                    <li><a href="#">Accessories,</a></li>
                                    <li><a href="#">Women,</a></li>
                                    <li><a href="#">Men,</a></li>
                                    <li><a href="#">Kid,</a></li>
                                </ul>
                            </div>';
    echo $html;
}

add_action('myrth_share_product', 'myrth_share_product');

function myrth_share_product() {
    $html = '<div class="sin__desc product__share__link">
                                <p><span>Share this:</span></p>
                                <ul class="pro__share">
                                    <li><a class="st-custom-button" data-network="twitter" target="_blank"><i class="icon-social-twitter icons"></i></a></li>
                                    <li><a class="st-custom-button" data-network="facebook" target="_blank"><i class="icon-social-facebook icons"></i></a></li>
                                    <li><a class="st-custom-button" data-network="googleplus" target="_blank"><i class="icon-social-google icons"></i></a></li>
                                </ul>
                            </div>';
    echo $html;
}

add_action('myrth_show_rating', 'myrth_show_rating', 10, 2);

function myrth_show_rating($product_id) {
    $html = '<ul class="rating">
                            <li><i class="icon-star icons"></i></li>
                            <li><i class="icon-star icons"></i></li>
                            <li><i class="icon-star icons"></i></li>
                            <li class="old"><i class="icon-star icons"></i></li>
                            <li class="old"><i class="icon-star icons"></i></li>
                        </ul>';
    echo $html;
}

add_action('myrth_show_notice_add_to_cart', 'myrth_show_notice_add_to_cart');

function myrth_show_notice_add_to_cart() {
   $html = '<div class="woocommerce woo-msg col-lg-12"></div>';
   echo $html;
}

add_action('myrth_show_notice_remove_item_in_cart', 'myrth_show_notice_remove_item_in_cart');

function myrth_show_notice_remove_item_in_cart(){
    if ($_GET['removed_item'] == 1) {
        $html = '<div class="woocommerce woo-msg col-lg-12 remove-notice"><div class="woocommerce-message" style="display: block">Item removed</div></div>';
        echo $html; 
    }
}