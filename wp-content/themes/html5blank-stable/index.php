<?php get_header() ?>


<!-- Start Slider Area -->

<?php get_template_part('template-parts/slideshow') ?>

<!-- End Slider Aread -->

<!-- Start Category Area -->
<section class="htc__product__area pb--70"> 
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section__title__with__cat__link">
                    <div class="section__title text--left">
                        <h2 class="title__line title__border">New Arrivals</h2>
                        <p># Trending This Season</p>
                    </div>
                    <div class="ht__gocat__link">
                        <a href="shop">Go To Categories</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="product__wrap clearfix">
               <?php get_template_part('template-parts/home', 'products') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ht__show__more__btn">
                    <a href="javascript:void(0)" data-page-current="1" >Show More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Category Area -->

<!-- Start Promo Box Area -->
<div class="htc__promobox__area pb--70">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="shop">
                    <div class="htc__promo__thumb" style="background: rgba(0, 0, 0, 0) url(<?php echo get_template_directory_uri() ?>/img/banner/bn-2/1.jpg) no-repeat scroll center center / cover ;">
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Promo Box Area -->

<?php get_template_part('template-parts/modal', 'newsletter'); ?>

<?php get_footer(); ?>