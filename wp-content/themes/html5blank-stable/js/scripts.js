jQuery(document).ready(function () {
    Builder.select_color();
    Builder.select_size();
    Builder.add_to_Cart();
    Builder.choose_color();
    Builder.trigger_to_shipping_address();
    Builder.filter_color();
    Builder.filter_size();
    Builder.close_panel_cart();
    Builder.trigger_update_cart();
    Builder.filter_price();
    //Builder.add_to_wishlisht();
    Builder.load_more();
    Builder.modal_subcribe();
});

var Builder = {
    
    modal_subcribe: function () {
        var current_date = new Date();
        current_date.setHours(23,59,59,999);
        console.log(current_date);
        if (jQuery.cookie('popup') == null) {
            jQuery('#modal-subcribe').modal('show').css('display', 'block');
            jQuery.cookie('popup', '1', {
                expires: current_date
            });
        }
    },
    
    load_more: function(){
        jQuery('body').on('click', '.ht__show__more__btn > a', function(){
            var page_current = parseInt(jQuery(this).attr('data-page-current')) + 1;
            var that = jQuery(this);
            jQuery.ajax({
                type: 'GET',
                url: '/wp-admin/admin-ajax.php',
                beforeSend: function (xhr) {
                    jQuery('.ht__show__more__btn > a').text('Loading...');
                },
                data: {
                    action: 'myrth_load_more',
                    page: page_current
                },
                success: function (data, textStatus, jqXHR) {
                    var data = jQuery.parseJSON(data);
                    if (data.status == 1) {
                        jQuery(data.html).appendTo('.product__wrap');
                        jQuery(that).attr('data-page-current', page_current)
                        jQuery('.ht__show__more__btn > a').text('Show More');
                    } else {
                        jQuery('.ht__show__more__btn').remove();
                    }
                    console.log(data);
                }
            });
        });
    },
    
    add_to_wishlisht: function(){
        jQuery('body').on('click', '.btn-add-to-wishlisht', function(){
            jQuery('#modal-notice').modal('show');
            return;
            var product_id = jQuery(this).attr('data');
            if (!product_id) {
                return;
            }
            jQuery.ajax({
                type: 'POST',
                url: path_ajax,
                beforeSend: function (xhr) {
                  jQuery('.remove-notice').remove();
                },
                data: {
                    action: 'myrth_add_to_wishlisht',
                    product_id: product_id
                },
                success: function (data, textStatus, jqXHR) {
                   if (data) {
                       data = jQuery.parseJSON(data);
                       console.log(data);
                   }
                }
            });
        });
    },
    
    filter_price: function(){
        jQuery('#btn-filter-price').click(function(){
            jQuery('.frmFilterPrice').submit();
        });
    },
    
    trigger_update_cart: function(){
        jQuery( document ).on( 'click', '.style__btn_update_cart', function(){
            var qty = 0
            jQuery('.woocommerce-cart-form__contents .qty').each(function(index){
               qty += parseInt(jQuery(this).val())  
            });
            jQuery('.htc__shopping__cart .htc__qua').text(qty)
        });
    },
    
    close_panel_cart: function(){
        jQuery('body').on('click', '.offsetmenu__close__btn', function(){
           jQuery('.shopping__cart').removeClass('shopping__cart__on');
           jQuery('.body__overlay').removeClass('is-visible');
        });
    },
    
    filter_color: function(){
        jQuery('body').on('click', '.ht__color__list li', function(){
            var color = jQuery(this).attr('class');
            jQuery('.input-color').val(color);
            jQuery('.frmFilterColor').submit();
        });
    },
    
    filter_size: function(){
        jQuery('body').on('click', '.ht__size__list li', function(){
            var size = jQuery(this).attr('class');
            jQuery('.input-size').val(size);
            jQuery('.frmFilterSize').submit();
        });
    },
    
    trigger_to_shipping_address: function () {
        jQuery('body').on('click', '.ship-to-another-trigger', function () {
            console.log(jQuery('#ship-to-different-address-checkbox').val());
            if (jQuery('#ship-to-different-address-checkbox').is(":checked")) {
                jQuery('#ship-to-different-address-checkbox').prop('checked', false)
                jQuery('.accordion .shipping_address').toggle(300)
            } else {
                jQuery('#ship-to-different-address-checkbox').prop('checked', true)
                jQuery('.accordion .shipping_address').toggle(300)
            }
        });
    },
    
    select_color: function () {
        jQuery('.htc__product__details__top .pro__color li a').click(function () {
            var data_color = jQuery(this).attr('data-color');
            var data_size = jQuery('.select__size option:selected').val();
            jQuery('#attr_color').val(data_color);
            if (data_color) {
                jQuery('.pro__color li a').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.portfolio-full-image .tab-pane').removeClass('in active');
                jQuery('.product__small__images li').removeClass('active');
                jQuery('.product__small__images li').hide();
                jQuery('.product__small__images li').each(function(index, item){
                    if (jQuery(item).attr('data-color') == data_color && jQuery(item).attr('data-size') == data_size) {
                        jQuery(item).css('display', 'inline-block');
                    }
                });
                jQuery('#img-tab-' + data_color + '-0').addClass('in').addClass('active');
                jQuery('#img-tab-' + data_color + '-' + data_size + '-0').addClass('in').addClass('active');
                jQuery('#nav-' + data_color + '-0').addClass('active');
                jQuery('#nav-' + data_color + '-' + data_size + '-0').addClass('active');
                // get attr size if have
                if (jQuery('.select__size').length > 0) {
                    var size = jQuery('.select__size option:selected').val();
                    jQuery('#attr_size').val(size);
                }
            }
            Builder.verify_button_add_to_cart();
        });
    },
    
//    select_color: function () {
//        jQuery('.htc__product__details__top .pro__color li a').click(function () {
//            var data_color = jQuery(this).attr('data-color');
//            jQuery('#attr_color').val(data_color);
//            if (data_color) {
//                jQuery('.pro__color li a').removeClass('active');
//                jQuery(this).addClass('active');
//                jQuery('.portfolio-full-image .tab-pane').removeClass('in active');
//                jQuery('.product__small__images li').removeClass('active');
//                jQuery('#img-tab-' + data_color).addClass('in').addClass('active');
//                jQuery('#nav-' + data_color).addClass('active');
//                // get attr size if have
//                if (jQuery('.select__size').length > 0) {
//                    var size = jQuery('.select__size option:selected').val();
//                    jQuery('#attr_size').val(size);
//                }
//            }
//            Builder.verify_button_add_to_cart();
//        });
//    },

    select_size: function () {
        jQuery('.htc__product__details__top .select__size').change(function () {
            var size = jQuery(this).val();
            jQuery('#attr_size').val(size);
            if (size) {
                var color = ''
                if (jQuery('.pro__color').length > 0) {
                    color = jQuery('.pro__color').find('.active').attr('data-color');
                    jQuery('#attr_color').val(color)
                }
                if (color){
                    // change gallery img with size & color
                    jQuery('.portfolio-full-image .tab-pane').removeClass('in active');
                    jQuery('.product__small__images li').removeClass('active');
                    jQuery('.product__small__images li').hide();
                    jQuery('.product__small__images li').each(function(index, item){
                        if (jQuery(item).attr('data-color') == color && jQuery(item).attr('data-size') == size) {
                            jQuery(item).css('display', 'inline-block');
                        }
                    });
                    jQuery('#img-tab-' + color + '-0').addClass('in').addClass('active');
                    jQuery('#img-tab-' + color + '-' + size + '-0').addClass('in').addClass('active');
                    jQuery('#nav-' + color + '-0').addClass('active');
                    jQuery('#nav-' + color + '-' + size + '-0').addClass('active');
                }
            }
            Builder.verify_button_add_to_cart();
        });
    },
    
    // on home page
    choose_color: function(){
        jQuery('body').on('click', '.wr__pro_color .pro__color li a', function(){
            console.log('ddd');
            var wrap = jQuery(this).parents().eq(1);
            jQuery(wrap).find('a').removeClass('active');
            jQuery(this).addClass('active');
            var product_id = jQuery(this).attr('data-product-id');
            var link_img = jQuery(this).attr('data-image');
            jQuery('#thumb-' + product_id).find('img').attr('src', link_img);
            jQuery('#thumb-list-' + product_id).find('img').attr('src', link_img);
        });    
    },

    verify_button_add_to_cart: function () {
        var flag = true;
        if (jQuery('#attr_size').length > 0) {
            if (jQuery('#attr_size').val() == '') {
                flag = false;
            }
        }

        if (jQuery('#attr_color').length > 0) {
            if (jQuery('#attr_color').val() == '') {
                flag = false;
            }
        }
        if (flag == true) {
            var check = jQuery('#status-stock').attr('data-status');
            if (check == 'instock') {
                jQuery('#wr__add_to_cart').show();
            }
        } else if (flag == false) {
            var check = jQuery('#status-stock').attr('data-status');
            if (check == 'outofstock') {
                jQuery('#wr__add_to_cart').hide();
            }
        }
    },

    add_to_Cart: function () {
        jQuery('.btn-add-to-cart').click(function () {
            jQuery.ajax({
                type: 'POST',
                url: path_ajax,
                beforeSend: function (xhr) {
                  jQuery('.remove-notice').remove();
                },
                data: {
                    action: 'myrth_add_to_cart',
                    product_id: jQuery('#input_product_id').val(),
                    attribute_pa_color: jQuery('#attr_color').val(),
                    attribute_pa_size: jQuery('#attr_size').val()
                },
                success: function (data, textStatus, jqXHR) {
                   if (data) {
                       data = jQuery.parseJSON(data);
                       console.log(data);
                       jQuery('.htc__qua').text(data.count);
                       jQuery('.woocommerce-message').remove();
                       jQuery(data.html_notice).appendTo('.woo-msg')
                       jQuery('.woocommerce-message').show();
                       jQuery('.shopping__cart__inner').empty();
                       jQuery(data.html_cart_panel).appendTo('.shopping__cart__inner');
                       jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                   }
                }
            });
        })
    },

};