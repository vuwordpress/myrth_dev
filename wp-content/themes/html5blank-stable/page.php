<?php get_header(); ?>

<?php get_template_part('template-parts/breadcrumb') ?>

<main role="main">
    <!-- section -->
    <section>

        <div class="container">

            <div class="row">
		<div class="wr-content">
		<div class="col-xs-12">
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <?php the_content(); ?>

                    <?php endwhile; ?>

                <?php else: ?>

                    <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

                <?php endif; ?>
		</div>
		</div>
            </div>

        </div>

    </section>
    <!-- /section -->
</main>

<?php get_footer(); ?>
