<?php /* Template Name: Cart Page Template */ get_header(); ?>


<!-- Start Bradcaump area -->
<?php get_template_part('template-parts/breadcrumb') ?>
<!-- End Bradcaump area -->

<main role="main">
    <!-- section -->
    <section>

        <div class="container">

            <div class="row">

                <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <?php the_content(); ?>

                    <?php endwhile; ?>

                <?php else: ?>

                    <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

                <?php endif; ?>

            </div>

        </div>

    </section>
    <!-- /section -->
</main>

<?php get_footer(); ?>
