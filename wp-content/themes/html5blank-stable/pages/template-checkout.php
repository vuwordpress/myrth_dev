<?php /* Template Name: Checkout Page Template */ get_header(); ?>


<!-- Start Bradcaump area -->
<?php get_template_part('template-parts/breadcrumb') ?>
<!-- End Bradcaump area -->

<main role="main">
    <!-- section -->
    <section>

        <div class="checkout-wrap ptb--70">

            <div class="container"> 

                <div class="row">

                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                            <!-- article -->
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
             
                                <?php the_content(); ?>

                            </article>
                            <!-- /article -->

                        <?php endwhile; ?>

                    <?php else: ?>

                        <!-- article -->
                        <article>

                            <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>

                </div>

            </div>

        </div>

    </section>
    <!-- /section -->
</main>

<?php get_footer(); ?>
