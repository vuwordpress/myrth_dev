<?php /* Template Name: Shop Page Template */ get_header(); ?>


<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area" style="background: rgba(0, 0, 0, 0) url(<?php echo get_template_directory_uri() ?>/img/bg/4.jpg) no-repeat scroll center center / cover ;">
    <div class="ht__bradcaump__wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="bradcaump__inner">
                        <nav class="bradcaump-inner">
                            <a class="breadcrumb-item" href="<?php echo home_url(); ?>">Home</a>
                            <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                            <span class="breadcrumb-item active">shopping cart</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- End Bradcaump area -->

<main role="main">
    <!-- section -->
    <section>

        <div class="container">

            <div class="row">

                <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <?php the_content(); ?>

                    <?php endwhile; ?>

                <?php else: ?>

                    <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

                <?php endif; ?>

            </div>

        </div>

    </section>
    <!-- /section -->
</main>

<?php get_footer(); ?>
