<?php get_header(); ?>
<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area" style="background: rgba(0, 0, 0, 0) url(<?php echo get_template_directory_uri() ?>/img/bg/4.jpg) no-repeat scroll center center / cover ;">
    <div class="ht__bradcaump__wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="bradcaump__inner">
                        <nav class="bradcaump-inner">
                            <a class="breadcrumb-item" href="<?php echo home_url(); ?>">Home</a>
                            <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                            <span class="breadcrumb-item active">Search</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Bradcaump area -->

<section class="htc__product__area pb--70"> 
    <div class="container">
        <div class="row">
            <div class="product__wrap clearfix">

                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <?php 
           
                        $regular_price = get_post_meta(get_the_ID(), '_regular_price', true);
                        $sale_price = get_post_meta(get_the_ID(), '_sale_price', true);
                        if (!$regular_price) {
                            $regular_price = get_post_meta(get_the_ID(), '_price', true);
                        }
                        //$product = new WC_Product_Variable(get_the_ID());
                        $product = wc_get_product(get_the_ID());
                        $attributes = $product->get_attributes();
                        $data_attributes = _prepare_data_attributes($attributes);
                        $colors = '';
                        $product_data = array();
                        $gallery = _prepare_data_gallery(get_the_ID());
                        if ($gallery) {
                            $product_data['images'] = $gallery;
                        }
                        if ($data_attributes) {
                            $product_data['data_attributes'] = $data_attributes;
                            $colors = myrth_get_pro__color($product_data); 
                        }
                        ?>
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                            <div id="product-<?php echo $product->id ?>" class="product">
                                <div class="product__thumb">
                                    <a id="thumb-<?php the_ID() ?>" href="<?php the_permalink() ?>">
                                        <?php if ($product->is_type('simple') == 1) : ?>
                                            <?php the_post_thumbnail('full'); ?>
                                        <?php else: ?>
                                            <img src="<?php echo reset($gallery) ?>" />
                                        <?php endif; ?>
                                    </a>
                                    <div class="product__hover__info">
                                        <ul class="product__action">
                                            <li class="add-wishlist">
                                                <a href="<?php echo '//' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>?add_to_wishlist=<?php the_ID(); ?>" rel="nofollow" data-product-id="<?php the_ID() ?>" data-product-type="variable" class="add_to_wishlist"><i class="icon-heart icons"></i></a>
                                            </li>
                                            <li><a href="<?php the_permalink() ?>"><i class="icon-handbag icons"></i></a></li>
                                            <li><a href="<?php the_permalink() ?>"><i class="icon-shuffle icons"></i></a></li>
                                        </ul>
                                    </div>
                                    <div style="clear:both"></div>
                                    <div class="yith-wcwl-wishlistaddresponse"></div>
                                </div>
                                <div class="product__inner">
                                    <div class="product__details">
                                        <h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
                                        <ul class="pro__prize">
                                            <?php if ($sale_price && $regular_price) : ?>
                                            <li class="old__prize"><?php echo wc_price($regular_price) ?></li>
                                            <li><?php echo wc_price($sale_price) ?></li>
                                            <?php else: ?>
                                            <li><?php echo wc_price($regular_price) ?></li>
                                            <?php endif; ?>
                                        </ul>
                                        <?php if ($colors) : ?>
                                        <div class="wr__pro_color">
                                            <ul class="pro__color">
                                                <?php foreach($colors as $key => $color_item) : ?>
                                                <li class="<?php echo $color_item['slug'] ?>"><a data-product-id="<?php the_ID() ?>" data-image="<?php echo $color_item['image'] ?>" data-color="<?php echo $color_item['slug'] ?>" href="javascript:void(0)">Grey</a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>

                <?php else: ?>

                    <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

                <?php endif; ?>

            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
