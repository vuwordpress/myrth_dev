<?php global $post; 
$banner =  get_post_meta($post->ID, 'wpcf-banner-page', true);
if (!$banner) {
    $banner = get_template_directory_uri() . '/img/bg/4.jpg';
}

$queried_object = get_queried_object();
$link = '';
if (is_product_category()) {
    $text = $queried_object->name;
} elseif (is_single()) {
    $term_obj = wp_get_post_terms($queried_object->ID, 'product_cat');
    if ($term_obj) {
        $text = $term_obj[0]->name;
        $link = get_term_link($term_obj[0]->term_id);
    } else {
        $text = $queried_object->name;
    }
} elseif (is_shop()) {
    $text = 'Shop';
} elseif (is_page()) {
    $text = $post->post_title;
}

?>

<div class="ht__bradcaump__area" style="background: rgba(0, 0, 0, 0) url(<?php echo $banner ?>) no-repeat scroll center center / cover ;">
    <div class="ht__bradcaump__wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="bradcaump__inner">
                        <nav class="bradcaump-inner">
                            <a class="breadcrumb-item" href="<?php echo home_url(); ?>">Home</a>
                            <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                            <a class="breadcrumb-item" href="<?php echo $link ?>"><?php echo $text; ?></a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>