<?php
   global $woocommerce;
   $carts = wc()->cart->get_cart_contents();
?>

<div class="shopping__cart">
    <div class="shopping__cart__inner">
        <div class="offsetmenu__close__btn">
            <a href="#"><i class="zmdi zmdi-close"></i></a>
        </div>
        <div class="shp__cart__wrap">
            <?php if (!empty($carts)) : ?>
            <?php foreach($carts as $cart_item_key => $cart_item) : ?>
            <?php
            $variation_product = wc_get_product($cart_item['variation_id']);
            $data_variation = $cart_item['data']->get_data();
            $attachment = wp_get_attachment_image_src($data_variation['image_id'], 'small-2');
            ?>
            <div class="shp__single__product">
                <div class="shp__pro__thumb">
                    <a href="<?php echo get_permalink($cart_item['variation_id']); ?>">
                        <img src="<?php echo $attachment[0] ?>" alt="product images">
                    </a>
                </div>
                <div class="shp__pro__details">
                    <h2><a href="<?php echo get_permalink($cart_item['variation_id']); ?>"><?php echo $data_variation['name']; ?></a></h2>
                    <span class="quantity">QTY: <?php echo $cart_item['quantity'] ?></span>
                    <?php //echo myrth_show_attributes($data_variation['attributes']) ?>
                    <span class="shp__price"><?php echo wc_price($cart_item['line_total']) ?></span>
                </div>
                <div class="remove__btn">
                    <a href="<?php echo esc_url(WC()->cart->get_remove_url($cart_item_key)) ?>" data-product-id="<?php echo $cart_item['product_id'] ?>" data-variation-id="<?php echo $cart_item['variation_id'] ?>" title="Remove this item"><i class="zmdi zmdi-close"></i></a>
                </div>
            </div>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <ul class="shoping__total">
            <li class="subtotal">Subtotal:</li>
            <li class="total__price"><?php echo wc_price( wc()->cart->get_cart_contents_total() ) ?></li>
        </ul>
        <ul class="shopping__btn">
            <li><a href="<?php echo home_url() ?>/cart">View Cart</a></li>
            <li class="shp__checkout"><a href="<?php echo home_url() ?>/checkout">Checkout</a></li>
        </ul>
    </div>
</div>