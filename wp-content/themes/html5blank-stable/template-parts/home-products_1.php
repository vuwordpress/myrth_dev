
<?php
$list_products = get_news_product();

foreach ($list_products as $key => $product) :
    ?>
<!-- Start Single Product -->
    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
        <div id="product-<?php echo $product['product_id'] ?>" class="product">
            <div class="product__thumb">
                <a id="thumb-<?php echo $product['product_id'] ?>" href="<?php echo get_permalink($product['product_id']) ?>">
                    <?php if ($product['images']) : ?>
                        <img src="<?php echo reset($product['images']) ?>" alt="product images">
                    <?php endif; ?>
                </a>
                <div class="product__hover__info">
                    <ul class="product__action">
                        <li class="add-wishlist">
                            <a href="<?php echo home_url() ?>/?add_to_wishlist=<?php echo $product['product_id'] ?>" rel="nofollow" data-product-id="<?php echo $product['product_id'] ?>" data-product-type="variable" class="add_to_wishlist"><i class="icon-heart icons"></i></a>
                        </li>
                        <li><a href="cart"><i class="icon-handbag icons"></i></a></li>
                        <li><a href="<?php echo get_permalink($product['product_id']) ?>"><i class="icon-shuffle icons"></i></a></li>
                    </ul>
                </div>
                <div style="clear:both"></div>
                <div class="yith-wcwl-wishlistaddresponse"></div>
            </div>
            <div class="product__inner">
                <div class="product__details">
                    <h2><a href="<?php echo get_permalink($product['product_id']) ?>"><?php echo $product['title'] ?></a></h2>
                    <ul class="pro__prize">
                        <?php if ($product['sale_price'] && $product['regular_price']) : ?>
                            <li class="old__prize"><?php echo $product['regular_price'] ?></li>
                            <li><?php echo $product['sale_price'] ?></li>
                        <?php else: ?>
                            <li><?php echo $product['regular_price'] ?></li>
                        <?php endif; ?>
                    </ul>
                    
                    <?php 
                    $colors = myrth_get_pro__color($product);
                    if ($colors) :
                    ?>
                    <div class="wr__pro_color">
                        <ul class="pro__color">
                            <?php foreach($colors as $key => $color_item) : ?>
                            <li class="<?php echo $color_item['slug'] ?>"><a data-product-id="<?php echo $product['product_id'] ?>" data-image="<?php echo $color_item['image'] ?>" data-color="<?php echo $color_item['slug'] ?>" href="javascript:void(0)">Grey</a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<!-- End Single Product -->
<?php endforeach; ?>
