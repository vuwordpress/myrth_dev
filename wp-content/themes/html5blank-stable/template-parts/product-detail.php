<?php global $product; ?>
<section class="htc__product__details bg__white ptb--70 product-<?php echo get_the_ID() ?>">
    <!-- Start Product Details Top -->
    <div class="htc__product__details__top">
        <div class="container">
            <div class="row">

                <?php do_action('myrth_show_notice_add_to_cart'); ?>

                <?php do_action('myrth_show_notice_remove_item_in_cart'); ?>   

                <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                    <?php do_action('myrth_show_product_gallery', get_the_ID()) ?>
                </div>

                <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12 smt-40 xmt-40">
                    <div class="ht__product__dtl">
                        <form name="frmAddToCart" method="GET" >
                            <h2><?php the_title() ?></h2>

<!--                            <h6>Model: <span>MNG001</span></h6>-->
   
                            <?php do_action('myrth_show_price', get_the_ID()) ?>

                            <div class="pro__info"><?php echo strip_tags(get_the_content(), '<strong>,<b>,<i>,a>, <p>, <br>') ?></div>

                            <div class="ht__pro__desc">

                                <div class="sin__desc">
                                    <?php 
                                    $check = get_post_meta(get_the_ID(), '_stock_status', true);
                                    $status = ($check == 'instock') ? 'In Stock' : 'Out Stock';
                                    ?>
                                    <p id="status-stock" data-status="<?php echo $check ?>"><span>Availability:</span> <?php echo $status; ?></p>
                                </div>

                                <?php do_action('myrth_show_color_variation', get_the_ID()) ?>

                                <?php do_action('myrth_show_size_variation', get_the_ID()) ?>
                                
                                <?php $display = ($product->is_type('simple') == 1) ? 'style="display: block"' : ''; ?>

                                <div id="wr__add_to_cart" class="sin__desc align--left" <?php echo $display ?>>
                                    <input type="hidden" name="act" value="add_to_cart" /> 
                                    <input type="hidden" id="input_product_id" name="product_id" value="<?php the_ID() ?>" />
                                    <a href="javascript:void(0)" class="btn-add-to-cart">Add To Cart</a>
                                </div>
                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-163">
                                    <div class="yith-wcwl-add-button show" style="display:block">
                                        <i class="icon-heart icons"></i>
                                        <a href="<?php echo '//' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>/?add_to_wishlist=<?php echo get_the_ID() ?>" rel="nofollow" data-product-id="<?php echo get_the_ID() ?>" data-product-type="variable" class="add_to_wishlist">Add to Wishlist</a>
                                        <img src="<?php echo home_url() ?>/wp-content/plugins/yith-woocommerce-wishlist/assets/images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden">
                                    </div>
                                    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                        <span class="feedback">Product added!</span>
                                        <a href="<?php echo home_url() ?>/wishlist/" rel="nofollow">Browse Wishlist</a>
                                    </div>
                                    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                        <span class="feedback">The product is already in the wishlist!</span>
                                        <a href="<?php echo home_url() ?>/wishlist/" rel="nofollow">Browse Wishlist</a>
                                    </div>
                                    <div style="clear:both"></div>
                                    <div class="yith-wcwl-wishlistaddresponse"></div>
                                </div>

                                <?php //do_action('myrth_show_categories_assigned', get_the_ID()) ?>

                                <?php //do_action('myrth_show_product_tag', get_the_ID()) ?>

                                <?php do_action('myrth_share_product', get_the_ID()) ?>

                            </div>
                        </form>
                    </div>
                </div>
                <?php do_action('woocommerce_after_single_product'); ?>
            </div>
        </div>
    </div>
    <!-- End Product Details Top -->
</section>