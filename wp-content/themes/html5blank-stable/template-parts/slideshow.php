<?php 
$args = array('post_type' => 'slider');
$sliders = get_posts($args);
if ($sliders) :
?>
<div class="slider__container slider--one">
    <div class="slide__container">
        <div class="container-fluid">
            <div class="row">
                <div class="slider__activation__wrap slider-activation-one no__navigation owl-carousel slider__fixed--height">
                    <?php foreach($sliders as $key => $slider) : ?>
                    <?php $slide_img = get_post_meta($slider->ID, 'wpcf-slide-image', true); ?>
                    <?php $slide_link = get_post_meta($slider->ID, 'wpcf-slide-link', true); ?>
                    <!-- Start Single Slide -->
                    <div class="col-md-4 col-lg-4 slider">
                        <a href="<?php echo $slide_link ?>">
                            <div class="slide">
                                <img src="<?php echo $slide_img ?>" alt="slider image">
<!--                                 <div class="slider__inner">
                                    <h1>#Men</h1>
                                    <p>Trending Of This Season</p>
                                </div> -->
                            </div>
                        </a>
                    </div>
                    <?php endforeach; ?>
                    <!-- End Single Slide -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>
