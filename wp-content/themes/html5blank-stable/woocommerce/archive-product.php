<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop');
?>

<!-- Start Bradcaump area -->
<?php get_template_part('template-parts/breadcrumb') ?>
<!-- End Bradcaump area -->

<section class="htc__product__grid bg__white ptb--70">
    <div class="container">
        <div class="row">

                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="htc__product__leftsidebar">
                        <h2 class="title__line--3">shop by</h2>
                        <!-- Start Category Area -->
                        <?php wc_get_template_part('sidebar', 'category'); ?>
                        <!-- End Category Area -->
                        <!-- Start Prize Range -->
                        <?php wc_get_template_part('filter', 'price'); ?>
                        <!-- End Prize Range -->
                        <?php global $woocommerce; 
                        //$attr_tax = $woocommerce->get_attribute_taxonomy_names();
                        ?>
                        <!-- Start Pro Color -->
                        <?php wc_get_template_part('filter', 'color'); ?>
                        <!-- End Pro Color -->
                        <!-- Start Pro Size -->
                        <?php wc_get_template_part('filter', 'size'); ?>
                        <!-- End Pro Size -->
                    </div>
                </div>

                <div class="col-md-9 col-sm-12 col-xs-12 smt-40 xmt-40">

                    <div class="htc__product__rightidebar">
                        <?php if ($_GET['removed_item']): ?>
                            <div class="woocommerce-message">Item removed</div>
                        <?php endif; ?>
                        <div class="htc__grid__top">
                            <div class="htc__select__option">
                                <?php
                                /**
                                 * woocommerce_before_shop_loop hook.
                                 *
                                 * @hooked wc_print_notices - 10
                                 * @hooked woocommerce_result_count - 20
                                 * @hooked woocommerce_catalog_ordering - 30
                                 */
                                do_action('woocommerce_before_shop_loop');
                                ?>
                            </div>
                            <div class="ht__pro__qun">
                                <?php
                                global $wp_query;
                                $paged = max(1, $wp_query->get('paged'));
                                $per_page = $wp_query->get('posts_per_page');
                                $total = $wp_query->found_posts;
                                $first = ( $per_page * $paged ) - $per_page + 1;
                                $last = min($total, $wp_query->get('posts_per_page') * $paged);

                                if ($total <= $per_page || -1 === $per_page) {
                                    echo '<span>Showing ' . $total . ' products</span>';
                                } else {
                                    echo '<span>Showing ' . $first . '-' . $last . ' of ' . $total . ' products</span>';
                                }
                                ?>
                            </div>
                            <!-- Start List And Grid View -->
                            <ul class="view__mode" role="tablist">
                                <li role="presentation" class="grid-view active"><a href="#grid-view" role="tab" data-toggle="tab" aria-expanded="true"><i class="zmdi zmdi-grid"></i></a></li>
                                <li role="presentation" class="list-view"><a href="#list-view" role="tab" data-toggle="tab" aria-expanded="false"><i class="zmdi zmdi-view-list"></i></a></li>
                            </ul>
                            <!-- End List And Grid View -->
                        </div>
                    </div>
                    <!-- Start Product View -->
                    <?php if (have_posts()) : ?>
                    <div class="row">
                        <div class="shop__grid__view__wrap">
    
                            <div role="tabpanel" id="list-view" class="single-grid-view tab-pane fade clearfix">
                                
                                <?php woocommerce_product_loop_start(); ?>

                                <?php while (have_posts()) : the_post(); ?>

                                    <?php
                                    /**
                                     * woocommerce_shop_loop hook.
                                     *
                                     * @hooked WC_Structured_Data::generate_product_data() - 10
                                     */
                                    do_action('woocommerce_shop_loop');
                                    
                                    global $product;
                                    
                                    ?>

                                    <?php wc_get_template_part('content', 'product-list'); ?>

                                <?php endwhile; // end of the loop. ?>

                                <?php woocommerce_product_loop_end(); ?>

                                <?php
                                /**
                                 * woocommerce_after_shop_loop hook.
                                 *
                                 * @hooked woocommerce_pagination - 10
                                 */
                                do_action('woocommerce_after_shop_loop');
                                ?>
                                
                            </div>
                            
                            <div role="tabpanel" id="grid-view" class="single-grid-view tab-pane fade clearfix active in">

                                <?php woocommerce_product_loop_start(); ?>

                                <?php while (have_posts()) : the_post(); ?>

                                    <?php
                                    /**
                                     * woocommerce_shop_loop hook.
                                     *
                                     * @hooked WC_Structured_Data::generate_product_data() - 10
                                     */
                                    do_action('woocommerce_shop_loop');
                                    ?>

                                    <?php wc_get_template_part('content', 'product'); ?>

                                <?php endwhile; // end of the loop. ?>

                                <?php woocommerce_product_loop_end(); ?>

                                <?php
                                /**
                                 * woocommerce_after_shop_loop hook.
                                 *
                                 * @hooked woocommerce_pagination - 10
                                 */
                                do_action('woocommerce_after_shop_loop');
                                ?>

                            </div>  
                        </div>
                    </div>
                    
                    <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>
                       
                                <?php
                                /**
                                 * woocommerce_no_products_found hook.
                                 *
                                 * @hooked wc_no_products_found - 10
                                 */
                                do_action('woocommerce_no_products_found');
                                ?>
                    <?php endif; ?>
                    <!-- End Product View -->
                </div>
        </div>
    </div>
</div>
</section>
<?php get_footer('shop'); ?>
