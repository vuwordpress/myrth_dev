<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop');
?>

<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area" style="background: rgba(0, 0, 0, 0) url(<?php echo get_template_directory_uri() ?>/img/bg/4.jpg) no-repeat scroll center center / cover ;">
    <div class="ht__bradcaump__wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="bradcaump__inner">
                        <nav class="bradcaump-inner">
                            <a class="breadcrumb-item" href="<?php echo home_url(); ?>">Home</a>
                            <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                            <span class="breadcrumb-item active">Products</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Bradcaump area -->

<section class="htc__product__grid bg__white ptb--70">
    <div class="container">
        <div class="row">

            <?php if (have_posts()) : ?>

                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="htc__product__leftsidebar">
                        <h2 class="title__line--3">shop by</h2>
                        <!-- Start Category Area -->
                        <div class="htc__category">
                            <h4 class="title__line--4">by categories</h4>
                            <?php $terms = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => false)) ?>
                            <?php if ($terms) : ?>
                            <?php  $queried_object = get_queried_object();?>
                            <ul class="ht__cat__list">
                                <?php foreach($terms as $key => $term) : ?>
                                <?php $class = ($queried_object->term_id == $term->term_id) ? 'class="active"' : '' ?>
                                <li <?php echo $class ?>><a href="<?php echo get_term_link($term) ?>"><?php echo $term->name; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                            <?php endif; ?>
                        </div>
                        <!-- End Category Area -->
                        <!-- Start Prize Range -->
                        <div class="htc-grid-range">
                            <h4 class="title__line--4">by price</h4>
                            <div class="content-shopby">
                                <div class="price_filter s-filter clear">
                                    <form action="#" method="GET">
                                        <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 20.4082%; width: 46.7347%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 20.4082%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 67.1429%;"></span></div>
                                        <div class="slider__range--output">
                                            <div class="price__output--wrap">
                                                <div class="price--output">
                                                    <span>Price :</span><input type="text" id="amount" readonly="">
                                                </div>
                                                <div class="price--filter">
                                                    <a href="#">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End Prize Range -->
                        <!-- Start Pro Color -->
                        <div class="ht__pro__color">
                            <h4 class="title__line--4">by color</h4>
                            <ul class="ht__color__list">
                                <li class="grey"><a href="#">grey</a></li>
                                <li class="lamon"><a href="#">lamon</a></li>
                                <li class="white"><a href="#">white</a></li>
                                <li class="red"><a href="#">red</a></li>
                                <li class="black"><a href="#">black</a></li>
                                <li class="pink"><a href="#">pink</a></li>
                            </ul>
                        </div>
                        <!-- End Pro Color -->
                        <!-- Start Pro Size -->
                        <div class="ht__pro__size">
                            <h4 class="title__line--4">By Size</h4>
                            <ul class="ht__size__list">
                                <li><a href="#">xs</a></li>
                                <li><a href="#">s</a></li>
                                <li><a href="#">m</a></li>
                                <li><a href="#">reld</a></li>
                                <li><a href="#">xl</a></li>
                            </ul>
                        </div>
                        <!-- End Pro Size -->
                    </div>
                </div>

                <div class="col-md-9 col-sm-12 col-xs-12 smt-40 xmt-40">

                    <div class="htc__product__rightidebar">
                        <div class="htc__grid__top">
                            <div class="htc__select__option">
                                <?php
                                /**
                                 * woocommerce_before_shop_loop hook.
                                 *
                                 * @hooked wc_print_notices - 10
                                 * @hooked woocommerce_result_count - 20
                                 * @hooked woocommerce_catalog_ordering - 30
                                 */
                                do_action('woocommerce_before_shop_loop');
                                ?>
                            </div>
                            <div class="ht__pro__qun">
                                <?php
                                global $wp_query;
                                $paged = max(1, $wp_query->get('paged'));
                                $per_page = $wp_query->get('posts_per_page');
                                $total = $wp_query->found_posts;
                                $first = ( $per_page * $paged ) - $per_page + 1;
                                $last = min($total, $wp_query->get('posts_per_page') * $paged);

                                if ($total <= $per_page || -1 === $per_page) {
                                    echo '<span>Showing ' . $total . ' products</span>';
                                } else {
                                    echo '<span>Showing ' . $first . '-' . $last . ' of ' . $total . ' products</span>';
                                }
                                ?>
                            </div>
                            <!-- Start List And Grid View -->
                            <ul class="view__mode" role="tablist">
                                <li role="presentation" class="grid-view active"><a href="#grid-view" role="tab" data-toggle="tab" aria-expanded="true"><i class="zmdi zmdi-grid"></i></a></li>
                                <li role="presentation" class="list-view"><a href="#list-view" role="tab" data-toggle="tab" aria-expanded="false"><i class="zmdi zmdi-view-list"></i></a></li>
                            </ul>
                            <!-- End List And Grid View -->
                        </div>
                    </div>
                    <!-- Start Product View -->
                    <div class="row">
                        <div class="shop__grid__view__wrap">
    
                            <div role="tabpanel" id="list-view" class="single-grid-view tab-pane fade clearfix">
                                
                                <?php woocommerce_product_loop_start(); ?>

                                <?php while (have_posts()) : the_post(); ?>

                                    <?php
                                    /**
                                     * woocommerce_shop_loop hook.
                                     *
                                     * @hooked WC_Structured_Data::generate_product_data() - 10
                                     */
                                    do_action('woocommerce_shop_loop');
                                    
                                    global $product;
                                    
                                    ?>

                                    <?php wc_get_template_part('content', 'product-list'); ?>

                                <?php endwhile; // end of the loop. ?>

                                <?php woocommerce_product_loop_end(); ?>

                                <?php
                                /**
                                 * woocommerce_after_shop_loop hook.
                                 *
                                 * @hooked woocommerce_pagination - 10
                                 */
                                do_action('woocommerce_after_shop_loop');
                                ?>
                                
                            </div>
                            
                            <div role="tabpanel" id="grid-view" class="single-grid-view tab-pane fade clearfix active in">

                                <?php woocommerce_product_loop_start(); ?>

                                <?php while (have_posts()) : the_post(); ?>

                                    <?php
                                    /**
                                     * woocommerce_shop_loop hook.
                                     *
                                     * @hooked WC_Structured_Data::generate_product_data() - 10
                                     */
                                    do_action('woocommerce_shop_loop');
                                    ?>

                                    <?php wc_get_template_part('content', 'product'); ?>

                                <?php endwhile; // end of the loop. ?>

                                <?php woocommerce_product_loop_end(); ?>

                                <?php
                                /**
                                 * woocommerce_after_shop_loop hook.
                                 *
                                 * @hooked woocommerce_pagination - 10
                                 */
                                do_action('woocommerce_after_shop_loop');
                                ?>

                            <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

                                <?php
                                /**
                                 * woocommerce_no_products_found hook.
                                 *
                                 * @hooked wc_no_products_found - 10
                                 */
                                do_action('woocommerce_no_products_found');
                                ?>

                            </div>  
                        </div>
                    </div>
                    <!-- End Product View -->
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</section>
<?php get_footer('shop'); ?>
