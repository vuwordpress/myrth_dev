<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if (empty($product) || !$product->is_visible()) {
    return;
}
$attributes = $product->get_attributes();
$data_attributes = _prepare_data_attributes($attributes);
$gallery = _prepare_data_gallery($product->id);
if ($data_attributes && $gallery) {
    $data['data_attributes'] = $data_attributes;
    $data['images'] = $gallery;
    $colors = myrth_get_pro__color($data);
}
?>
<div class="col-xs-12">
    <div class="ht__list__wrap">
        <div class="ht__list__product">
            <div class="ht__list__thumb">
                <a id="thumb-list-<?php echo $product->id ?>" href="<?php echo get_permalink($product->id) ?>">
                    <?php if ($product->is_type('simple') == 1) : ?>
                        <?php echo get_the_post_thumbnail($product->id, 'full'); ?>
                    <?php else: ?>
                        <img src="<?php echo reset($data['images']) ?>" alt="product images">
                    <?php endif; ?>
                </a>
            </div>
            <div class="htc__list__details">
                <h2><a href="<?php echo get_permalink($product->id) ?>"><?php echo $product->name; ?></a></h2>
                <ul class="pro__prize">
                    <?php if ($product->sale_price && $product->regular_price) : ?>
                        <li class="old__prize"><?php echo wc_price($product->regular_price) ?></li>
                        <li><?php echo wc_price($product->sale_price) ?></li>
                    <?php else: ?>
                        <li><?php echo wc_price($product->price) ?></li>
                    <?php endif; ?>
                </ul>
                <p><?php echo strip_tags($product->description, '<strong>,<b>,<i>,<a>,<p>,<h3>,<br>') ?></p>
                <?php if ($colors): ?>
                    <div class="wr__pro_color">
                        <span class="text-color">Color:</span> 
                        <ul class="pro__color">
                            <?php foreach ($colors as $key => $color_item) : ?>
                                <li class="<?php echo $color_item['slug'] ?>"><a data-product-id="<?php echo $product->id ?>" data-image="<?php echo $color_item['image'] ?>" data-color="<?php echo $color_item['slug'] ?>" href="javascript:void(0)"></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <ul class="product__action">
                    <li class="ht__addto__cart">
                        <a class="active" href="<?php echo get_permalink($product->id) ?>"><i class="icon-shuffle icons"></i>View Product</a>
                    </li>
                    <li><a href="<?php echo '//' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>?add_to_wishlist=<?php echo $product->id ?>" rel="nofollow" data-product-id="<?php echo $product->id ?>" data-product-type="variable" class="add_to_wishlist"><i class="icon-heart icons"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>