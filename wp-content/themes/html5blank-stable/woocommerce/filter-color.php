<?php $terms = get_terms(array('taxonomy' => 'pa_color', 'hide_empty' => false)) ?>
<?php if($terms) : ?>
<div class="ht__pro__color">
    <?php $queried_object = get_queried_object(); 
    $shop_url = home_url() . '/shop';
    $url = (!$queried_object->term_id) ? $shop_url : get_term_link($queried_object->term_id);
    ?>
    <form name="frmFilterColor" class="frmFilterColor" action="<?php echo $url ?>">
        <input type="hidden" class="input-color" name="color" value="" />
        <h4 class="title__line--4">by color</h4>
        <ul class="ht__color__list">
            <?php foreach($terms as $key => $term) : ?>
            <li class="<?php echo $term->slug ?>"><a href="javascript:void(0)"><?php echo $term->slug ?></a></li>
            <?php endforeach; ?>
        </ul>
    </form>
</div>
<?php endif; ?>