<?php 
$queried_object = get_queried_object();
$shop_url = home_url() . '/shop';
$url = (!$queried_object->term_id) ? $shop_url : get_term_link($queried_object->term_id);
//$str = '';
if ($_GET['min_price'] && $_GET['max_price']) {
    $min_price = $_GET['min_price'];
    $max_price = $_GET['max_price'];
    //$str = '$'. $_GET['min_price'] . ' - $' . $_GET['max_price'];
}
?>

<div class="htc-grid-range">
    <h4 class="title__line--4">by price</h4>
    <div class="content-shopby">
        <div class="price_filter s-filter clear">
            <form class="frmFilterPrice" action="<?php echo $url ?>" method="GET">
                <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 20.4082%; width: 46.7347%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 20.4082%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 67.1429%;"></span></div>
                <div class="slider__range--output">
                    <div class="price__output--wrap">
                        <div class="price--output">
                            <span>Price :</span><input type="text" data-min-price="<?php echo $min_price ?>" data-max-price="<?php echo $max_price ?>" id="amount" value="<?php echo $str; ?>" readonly="">
                            <input name="min_price" type="hidden" id="min-price" value="" readonly="">
                            <input name="max_price" type="hidden" id="max-price" value="" readonly="">
                        </div>
                        <div class="price--filter">
                            <a id="btn-filter-price" href="javascript:void(0)">Filter</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>