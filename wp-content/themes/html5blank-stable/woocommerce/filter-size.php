<?php $terms = get_terms(array('taxonomy' => 'pa_size', 'hide_empty' => false)) ?>
<?php if ($terms) : ?>
    <div class="ht__pro__size">
        <?php $queried_object = get_queried_object(); 
        $shop_url = home_url() . '/shop';
        $url = (!$queried_object->term_id) ? $shop_url : get_term_link($queried_object->term_id);
        ?>
        <form name="frmFilterSize" class="frmFilterSize" action="<?php echo $url ?>">
            <input type="hidden" name="size" class="input-size" value="" />
            <h4 class="title__line--4">By Size</h4>
            <ul class="ht__size__list">
                <?php foreach ($terms as $key => $term) : ?>
                <li class="<?php echo $term->slug ?>"><a href="javascript:void(0)"><?php echo $term->slug ?></a></li>
                <?php endforeach; ?>
            </ul>
        </form>
    </div>
<?php endif; ?>