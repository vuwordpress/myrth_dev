<div class="htc__category">
    <h4 class="title__line--4">by categories</h4>
    <?php $terms = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => false)) ?>
    <?php if ($terms) : ?>
        <?php $queried_object = get_queried_object(); ?>
        <ul class="ht__cat__list">
            <?php foreach ($terms as $key => $term) : ?>
                <?php $class = ($queried_object->term_id == $term->term_id) ? 'class="active"' : '' ?>
                <li <?php echo $class ?>><a href="<?php echo get_term_link($term) ?>"><?php echo $term->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>