<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author 	WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop');
?>

<!-- Start Bradcaump area -->

<?php get_template_part('template-parts/breadcrumb') ?>

<!-- End Bradcaump area -->

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <!-- Start Product Details Area -->

        <?php get_template_part('template-parts/product', 'detail') ?>

        <!-- End Product Details Area -->


        <!-- Start Product Description -->

        <?php //get_template_part('template-parts/product', 'description') ?>

        <!-- End Product Description -->

    <?php endwhile; ?>

<?php else: ?>

    <!-- article -->
    <article>
        <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>
    </article>
    <!-- /article -->

<?php endif; ?>


<?php
get_footer('shop');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
